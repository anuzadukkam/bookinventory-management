/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bookinventorymanagement.service.impl;
import in.ac.gpckasaragod.bookinventorymanagement.service.ItemService;
import in.ac.gpckasaragod.bookinventorymanagement.ui.model.data.BillItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class ItemServiceImpl extends ConnectionServiceImpl implements ItemService{
   public String  saveBillItem(String billno,Date bDate,String billamount){
      try{
          Connection connection = getConnection();
          Statement statement = connection.createStatement();
          SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
          String formatedDate= dateFormat.format(bDate);
          String query = "INSERT INTO BILL_ITEM (BILL_NO,BILL_DATE,BILL_AMOUNT ) VALUES" + "('"+billno+ "','"+formatedDate+ "','"+billamount+"')";
          System.err.println("Query:"+query);
          int status = statement.executeUpdate(query);
          if(status!=1){
              return "save failed";
          }else{
              return "saved successfully";
     
          }
      }catch (SQLException ex){
          Logger.getLogger(ItemServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
          return "save failed";
      } 
   }

    @Override
    public BillItem readBillItem(Integer Id) {
        BillItem billItem = null;
        try{
            Connection connection = getConnection();
          Statement statement = connection.createStatement();
          String query = "SELECT * FROM BILL_ITEM WHERE ID="+Id;
          ResultSet resultSet = statement.executeQuery(query);
          while(resultSet.next()){
              int id = resultSet.getInt("ID");
               String billNo = resultSet.getString("BILL_NO");
               Date bDate = resultSet.getDate("BILL_DATE");
               String billAmount = resultSet.getString("BILL_AMOUNT");
              billItem = new BillItem(id,billNo,bDate,billAmount);
              
          }
        }catch (SQLException ex){
          Logger.getLogger(ItemServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
      }
      return billItem;
    }

    public List<BillItem> getAllBillItems() {
        List<BillItem> billItems = new ArrayList<>();
        try{
             Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM BILL_ITEM";
        ResultSet resultSet = statement.executeQuery(query);
        
          while(resultSet.next()){
              int id = resultSet.getInt("ID");
              String billNo = resultSet.getString("BILL_NO");
               Date biDate = resultSet.getDate("BILL_DATE");
               String billAmount = resultSet.getString("BILL_AMOUNT");
              BillItem billItem = new BillItem(id,billNo,biDate,billAmount);
              billItems.add(billItem);
          }
        }catch(SQLException ex) {
            Logger.getLogger(ItemServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
        }
           return billItems;
    }

    @Override
    public String updateBillItem(Integer id, String billNo, Date bDate, String billAmount) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String formatedDate= dateFormat.format(bDate);
            String query = "UPDATE BILL_ITEM SET BILL_NO='"+billNo+"',BILL_DATE='"+formatedDate+"',BILL_AMOUNT='"+billAmount+"' WHERE ID="+id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
                return "Update failed";
            else
                return "Update successfully";
        }catch(SQLException ex){
            return "update failed";
        }
    }

    @Override
    public String deleteBillItem(Integer id) {
        try{
            Connection connection = getConnection();
            String query = "DELETE FROM BILL_ITEM WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
        }catch (SQLException ex) {
           return "Delete failed";
        }
        
    }
    
}

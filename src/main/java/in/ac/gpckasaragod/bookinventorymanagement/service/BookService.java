/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.bookinventorymanagement.service;

import in.ac.gpckasaragod.bookinventorymanagement.ui.model.data.BookShop;
import java.util.Date;

import java.util.List;

/**
 *
 * @author student
 */
public interface BookService {
    public String saveBookShop(String bookname, Date bDate, Float total);
    public BookShop readBookShop(Integer id);
    public List<BookShop> getAllBookShops();
    public String updateBookShop(Integer id,String bookName,Date bDate,Float total);
    public String deleteBookShop(Integer id);

    
    
    
}

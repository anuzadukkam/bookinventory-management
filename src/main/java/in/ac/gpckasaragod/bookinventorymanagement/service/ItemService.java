/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.bookinventorymanagement.service;

import in.ac.gpckasaragod.bookinventorymanagement.ui.model.data.BillItem;
import java.util.Date;
import java.util.List;

/**
 *
 * @author student
 */
public interface ItemService {
    public String saveBillItem(String billno,Date bDate,String billamount);
    public BillItem readBillItem(Integer Id);
    public List<BillItem> getAllBillItems();
    public String updateBillItem(Integer id,String billNo,Date bDate,String billAmount);
    public String deleteBillItem(Integer id);


}

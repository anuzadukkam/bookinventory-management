/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bookinventorymanagement.service.impl;

import in.ac.gpckasaragod.bookinventorymanagement.service.BookService;
import in.ac.gpckasaragod.bookinventorymanagement.ui.model.data.BookShop;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class BookServiceImpl extends ConnectionServiceImpl implements BookService{
  public String saveBookShop(String bookname, Date  bDate, Float total) {
      try{
          Connection connection = getConnection();
          Statement statement = connection.createStatement();
          SimpleDateFormat dbdateFormat = new SimpleDateFormat("yyyy-MM-dd");
          String  formatedDate = dbdateFormat.format(bDate);
          String query = "INSERT INTO BOOKSHOP (BOOK_NAME,BDATE,TOTAL) VALUES" + "('"+bookname+ "','"+formatedDate+ "','"+total+"')";
          System.err.println("Query:"+query);
          int status = statement.executeUpdate(query);
          if(status!=1){
              return "save failed";
          }else{
              return "saved successfully";
     
          }
      }catch (SQLException ex){
          Logger.getLogger(BookServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
          return "save failed";
      }
  }
  @Override
  public BookShop readBookShop(Integer id) {
      BookShop bookShop = null;
      try{
          Connection connection = getConnection();
          Statement statement = connection.createStatement();
          String query = "SELECT * FROM BOOKSHOP WHERE ID="+id;
          ResultSet resultSet = statement.executeQuery(query);
          while(resultSet.next()){
               String bookName = resultSet.getString("BOOK_NAME");
              Date bDate = resultSet.getDate("BDATE");
              Float total = resultSet.getFloat( "TOTAL");
              bookShop = new BookShop(id,bookName,bDate,total);
              
          }
      }catch (SQLException ex){
          Logger.getLogger(BookServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
      }
      return bookShop;
  }
  @Override
    public List<BookShop> getAllBookShops(){
        List<BookShop> bookShops = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM BOOKSHOP";
        ResultSet resultSet = statement.executeQuery(query);
        
          while(resultSet.next()){
              Integer id = resultSet.getInt("ID");
              String bookName = resultSet.getString("BOOK_NAME");
              Date billDate = resultSet.getDate("BDATE");
              Float total = resultSet.getFloat( "TOTAL");
              BookShop bookShop = new BookShop(id,bookName,billDate,total);
              bookShops.add(bookShop);
          }
        }  catch(SQLException ex) {
            Logger.getLogger(BookServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
        }
           return bookShops;
    }
    public String updateBookShop(Integer id,String bookName,Date bDate,Float total) {                    
             try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            SimpleDateFormat dbdateFormat = new SimpleDateFormat("yyyy-MM-dd");
          String  formatedDate = dbdateFormat.format(bDate);
            String query = "UPDATE BOOKSHOP SET BOOK_NAME='"+bookName+"',BDATE='"+formatedDate+"',TOTAL='"+total+"' WHERE ID="+id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
                return "Update failed";
            else
                return "Update successfully";
        }catch(SQLException ex){
            return "update failed";
        }
           
     }
    
     @Override
    public String deleteBookShop(Integer id) {
         try {
            Connection connection = getConnection();
            String query = "DELETE FROM BOOKSHOP WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (SQLException ex) {
           return "Delete failed";
        }
            
        }
  }

    

          
      
                  
     

          
            
        
    


/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bookinventorymanagement.model.ui;

import java.util.Date;

/**
 *
 * @author student
 */
public class BookshopUiModel {
    private Integer id;
    private String bookName;
    private Date bDate;
    private Float total;
    private String billNo;
    private Date billDate;
    private String billAmount;
     private String billId;
    private String bookId;
    private Date billlDate;
    private String qty;
    private String totalCost;
    public String getBookId;

    public BookshopUiModel(Integer id, String bookName, Date bDate, Float total, String billNo, Date billDate, String billAmount, String billId, String bookId, Date billlDate, String qty, String totalCost, String getBookId) {
        this.id = id;
        this.bookName = bookName;
        this.bDate = bDate;
        this.total = total;
        this.billNo = billNo;
        this.billDate = billDate;
        this.billAmount = billAmount;
        this.billId = billId;
        this.bookId = bookId;
        this.billlDate = billlDate;
        this.qty = qty;
        this.totalCost = totalCost;
        this.getBookId = getBookId;
    }

    public BookshopUiModel(int id, String billId, String bookId, Date bDate, String qty, String totalCost, String billName, Date billDate, Float total, String billNo, Date biDate, String billAmount) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Date getbDate() {
        return bDate;
    }

    public void setbDate(Date bDate) {
        this.bDate = bDate;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(String billAmount) {
        this.billAmount = billAmount;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public Date getBilllDate() {
        return billlDate;
    }

    public void setBilllDate(Date billlDate) {
        this.billlDate = billlDate;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getGetBookId() {
        return getBookId;
    }

    public void setGetBookId(String getBookId) {
        this.getBookId = getBookId;
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bookinventorymanagement.ui.model.data;

import java.util.Date;



/**
 *
 * @author student
 */
public class BillItem {
    private Integer id;
    private String billNo;
    private Date billDate;
    private String billAmount;

    public BillItem(Integer id, String billNo, Date billDate, String billAmount) {
        this.id = id;
        this.billNo = billNo;
        this.billDate = billDate;
        this.billAmount = billAmount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(String billAmount) {
        this.billAmount = billAmount;
    }

    
}
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bookinventorymanagement.ui.model.data;

import java.util.Date;


/**
 *
 * @author student
 */
public class BookShop {
    private Integer id;
    private String bookName;
    private Date bDate;
    private Float total;

    public BookShop(Integer id, String bookName, Date bDate, Float total) {
        this.id = id;
        this.bookName = bookName;
        this.bDate = bDate;
        this.total = total;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Date getbDate() {
        return bDate;
    }

    public void setbDate(Date bDate) {
        this.bDate = bDate;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

 

    
    
    
    
}
